package com.example.e_station.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_station.R;
import com.example.e_station.models.BookingModel;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class CustomBookingAdapter  extends RecyclerView.Adapter<CustomBookingAdapter.ViewHolder> {

    Context context ;
    ArrayList<BookingModel> bookingList;
//    private ClickListener clickListener;
    public static String PENDING= "0";
    public static String ACCEPTED= "1";
    public static String COMPLETE= "2";
    public static String REJECTED= "-1";

    public CustomBookingAdapter(Context context, ArrayList<BookingModel> bookingList) {
        this.context = context;
        this.bookingList = bookingList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.booking_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.eStationName.setText(bookingList.get(position).geteStationName());
        holder.eStationAddress.setText(bookingList.get(position).geteStationAddress());
        holder.vehicleNameModel.setText(bookingList.get(position).getVehicleName()+"/"+bookingList.get(position).getVehicleModel());
        holder.dateTime.setText(bookingList.get(position).getDate()+" "+bookingList.get(position).getTimeSlot());
        holder.rate.setText(bookingList.get(position).getRate());
        holder.orderId.setText(bookingList.get(position).getOrderId());

        String status = bookingList.get(position).getStatus();
        if(status.equalsIgnoreCase(PENDING)){
            holder.status.setText("PENDING");
        }else if(status.equalsIgnoreCase(ACCEPTED)){
            holder.status.setText("ACCEPTED");
        }else if (status.equalsIgnoreCase(COMPLETE)){
            holder.status.setText("COMPLETED");
        }else if(status.equalsIgnoreCase(REJECTED)){
            holder.status.setText("REJECTED");
        }


        /*holder.tvContactBuilder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+propertyModels.get(position).getContactBuilder()));
                context.startActivity(intent);

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        ImageView imageView ;
        TextView eStationName ,eStationAddress,dateTime,vehicleNameModel,rate,status,orderId;
//        ClickListener clickListener;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            eStationName = itemView.findViewById(R.id.eStationName);
            eStationAddress = itemView.findViewById(R.id.eStationAddress);
            dateTime = itemView.findViewById(R.id.dateTime);
            vehicleNameModel = itemView.findViewById(R.id.vehicleNameModel);
            rate = itemView.findViewById(R.id.rate);
            orderId = itemView.findViewById(R.id.orderId);
            status = itemView.findViewById(R.id.status);


//            this.clickListener = clickListener;
//            itemView.setOnClickListener(this);

        }

       /* @Override
        public void onClick(View view) {
            clickListener.onClick(getAdapterPosition());

        }*/
    }




   /* public interface ClickListener {
        void onClick(int position);

    }*/

    public String  convertPrice(double value) {
        String price ="";
        double val = Math.abs(value);
        if (val >= 10000000) {
            double s = (val / 10000000);
            price =  (new DecimalFormat("##.##").format(s)) + " Cr";
        } else if (val >= 100000) {
            double s = (val / 100000);
            price =  (new DecimalFormat("##.##").format(s)) + " Lac";
        }else {
            price = value+"";
        }
        return price;
    }
}
