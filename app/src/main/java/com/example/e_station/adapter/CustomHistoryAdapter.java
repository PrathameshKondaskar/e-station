package com.example.e_station.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.e_station.R;
import com.example.e_station.models.BookingModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CustomHistoryAdapter extends RecyclerView.Adapter<CustomHistoryAdapter.ViewHolder> {

    Context context ;
    ArrayList<BookingModel> bookingList;
//    private ClickListener clickListener;
    public static String PENDING= "0";
    public static String ACCEPTED= "1";
    public static String COMPLETE= "2";
    public static String REJECTED= "-1";

    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;

    public CustomHistoryAdapter(Context context, ArrayList<BookingModel> bookingList) {
        this.context = context;
        this.bookingList = bookingList;
        mFireStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.manager_history_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.eStationName.setText(bookingList.get(position).geteStationName());
        holder.vehicleNameModel.setText(bookingList.get(position).getVehicleName()+"/"+bookingList.get(position).getVehicleModel());
        holder.date.setText(bookingList.get(position).getDate());
        holder.rate.setText("Rs."+bookingList.get(position).getRate());
        holder.orderId.setText(bookingList.get(position).getOrderId());
        holder.payment.setText(bookingList.get(position).getPaymentType());
        holder.name.setText(bookingList.get(position).getUserName());

        String status = bookingList.get(position).getStatus();

        if(bookingList.get(position).getStatus().equalsIgnoreCase(PENDING)){
           holder.b2.setText("ACCEPTED");
        }else if(bookingList.get(position).getStatus().equalsIgnoreCase(ACCEPTED)){
            holder.b2.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            holder.b2.setText("DONE");
            holder.b1.setBackgroundColor(context.getResources().getColor(R.color.colorPrimaryDark));
            holder.b1.setText("CALL CUSTOMER");
        }else if(bookingList.get(position).getStatus().equalsIgnoreCase(COMPLETE)){
            holder.b1.setVisibility(View.GONE);
            holder.b2.setBackgroundDrawable(null);
            holder.b2.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
            holder.b2.setText("COMPLETED!!!");
        }

        holder.b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(status.equalsIgnoreCase(PENDING)){

                    holder.b1.setVisibility(View.GONE);
                    Map<String, Object> map = new HashMap<>();
                    map.put("status", "-1");


                    mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL)
                            .document(bookingList.get(position).getDocId())
                            .update(map)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });

                }else{
                    Intent intent = new Intent(Intent.ACTION_DIAL);
                    intent.setData(Uri.parse("tel:"+bookingList.get(position).getMobile()));
                    context.startActivity(intent);
                }
            }
        });

        holder.b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (status.equalsIgnoreCase(PENDING)) {
                    holder.b2.setText("DONE");
                    Map<String, Object> map = new HashMap<>();
                    map.put("status", "1");


                    mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL)
                            .document(bookingList.get(position).getDocId())
                            .update(map)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                }else if(status.equalsIgnoreCase(ACCEPTED)){
                    holder.b2.setText("DONE");
                    Map<String, Object> map = new HashMap<>();
                    map.put("status", "2");


                    mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL)
                            .document(bookingList.get(position).getDocId())
                            .update(map)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });
                }else if(status.equalsIgnoreCase(COMPLETE)){
                    holder.b2.setBackgroundDrawable(null);
                    holder.b2.setText("COMPLETED!!!");
                    holder.b2.setTextColor(context.getResources().getColor(R.color.colorPrimaryDark));
                    Map<String, Object> map = new HashMap<>();
                    map.put("status", "3");


                    mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL)
                            .document(bookingList.get(position).getDocId())
                            .update(map)
                            .addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {

                                }
                            });

                }
            }
        });



        /*holder.tvContactBuilder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:"+propertyModels.get(position).getContactBuilder()));
                context.startActivity(intent);

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return bookingList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        TextView eStationName , date,vehicleNameModel,rate,orderId,payment,name;
        Button b1,b2;
//        ClickListener clickListener;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            eStationName = itemView.findViewById(R.id.eStationName);
            date = itemView.findViewById(R.id.date);
            vehicleNameModel = itemView.findViewById(R.id.vehicleNameModel);
            rate = itemView.findViewById(R.id.rate);
            orderId = itemView.findViewById(R.id.orderId);
            payment = itemView.findViewById(R.id.paymentmethod);
            name = itemView.findViewById(R.id.name);
            b1 = itemView.findViewById(R.id.b1);
            b2 = itemView.findViewById(R.id.b2);



//            this.clickListener = clickListener;
//            itemView.setOnClickListener(this);

        }

       /* @Override
        public void onClick(View view) {
            clickListener.onClick(getAdapterPosition());

        }*/
    }




   /* public interface ClickListener {
        void onClick(int position);

    }*/

    public String  convertPrice(double value) {
        String price ="";
        double val = Math.abs(value);
        if (val >= 10000000) {
            double s = (val / 10000000);
            price =  (new DecimalFormat("##.##").format(s)) + " Cr";
        } else if (val >= 100000) {
            double s = (val / 100000);
            price =  (new DecimalFormat("##.##").format(s)) + " Lac";
        }else {
            price = value+"";
        }
        return price;
    }
}
