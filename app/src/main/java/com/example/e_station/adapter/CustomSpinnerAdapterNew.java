package com.example.e_station.adapter;

import android.content.Context;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.e_station.R;
import com.example.e_station.models.PowerStationModel;

import java.util.ArrayList;


public class CustomSpinnerAdapterNew extends ArrayAdapter<PowerStationModel> implements SpinnerAdapter {
    Context context;
    ArrayList<PowerStationModel> powerStationModels = new ArrayList<>();

    public CustomSpinnerAdapterNew(Context context, int resource, @NonNull ArrayList<PowerStationModel> objects) {
        super(context, resource, objects);
        this.context = context;
        this.powerStationModels = objects;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {


        View view = super.getView(position, convertView, parent);

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setTextColor(context.getResources().getColor(R.color.black));
        textView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
        textView.setGravity(Gravity.CENTER);
        return view;
    }

    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        convertView = LayoutInflater.from(context).inflate(R.layout.slot_item, parent, false);
        TextView textView = (TextView) convertView.findViewById(R.id.tvName);
        String x = powerStationModels.get(position).getName();
        textView.setText(x);

        return convertView;
    }
}
