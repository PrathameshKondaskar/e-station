package com.example.e_station;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSharedPreference {


    private final String FILENAME = "RealEstateApp";
    private final String KEY_ROLE = "role";
    private final String KEY_ADDRESS = "address";
    private final String KEY_MOBILE = "mobile";
    private final String KEY_TOKEN = "token";
    private final String KEY_USERID= "userId";
    private final String KEY_USERNAME= "userName";
    private final String KEY_ESTATIONNAME= "eStationName";
    private final String KEY_ESTATIONID= "eStationId";
    private final String KEY_ESTATIONADDRESS= "eStationAddress";

    // VARIABLES
    private SharedPreferences sharedPreferences;

    public UserSharedPreference(Context context) {
        sharedPreferences = context.getSharedPreferences(FILENAME, Context.MODE_PRIVATE);
    }

    private void putValue(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public void setRole(String role) {
        putValue(KEY_ROLE, role);
    }

    public String getRole() {
        return sharedPreferences.getString(KEY_ROLE, "");
    }


    public void setAddress(String address) {
        putValue(KEY_ADDRESS, address);
    }

    public String getAddress() {
        return sharedPreferences.getString(KEY_ADDRESS, null);
    }

    public void setMobile(String mobile) {
        putValue(KEY_MOBILE, mobile);
    }

    public String getMobile() {
        return sharedPreferences.getString(KEY_MOBILE, null);
    }

    public void setToken (String token)
    {
        putValue(KEY_TOKEN, token);

    }
    public String getToken()
    {
        return sharedPreferences.getString(KEY_TOKEN,null);
    }

    public void setUserId (String userId)
    {
        putValue(KEY_USERID, userId);

    }
    public String getUserId()
    {
        return sharedPreferences.getString(KEY_USERID,null);
    }
    public void setUserName (String userName)
    {
        putValue(KEY_USERNAME, userName);

    }
    public String getUserName()
    {
        return sharedPreferences.getString(KEY_USERNAME,null);
    }

    public void setEstationName (String estationName)
    {
        putValue(KEY_ESTATIONNAME, estationName);

    }
    public String getEstationName()
    {
        return sharedPreferences.getString(KEY_ESTATIONNAME,null);
    }

    public void setEstationId (String estationId)
    {
        putValue(KEY_ESTATIONID, estationId);

    }
    public String getEstationId()
    {
        return sharedPreferences.getString(KEY_ESTATIONID,null);
    }

    public void setEstationAddress (String estationId)
    {
        putValue(KEY_ESTATIONADDRESS, estationId);

    }
    public String getEstationAddress()
    {
        return sharedPreferences.getString(KEY_ESTATIONADDRESS,null);
    }






    public void clearSharedPreferences() {
        setAddress("");
        setRole("");
        setMobile("");
        setToken("");
        setEstationId("");
        setEstationName("");
        setEstationAddress("");
        setUserId("");
    }
}
