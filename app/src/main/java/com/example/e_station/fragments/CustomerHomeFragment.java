package com.example.e_station.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.e_station.R;
import com.example.e_station.activities.manager.AddVehicleActivity;
import com.example.e_station.activities.user.FeedbackActivity;
import com.example.e_station.activities.user.NewBookingActivity;
import com.example.e_station.activities.user.UserViewBookingActivity;

public class CustomerHomeFragment extends Fragment {

    CardView newBooking , viewBooking ,provideFeedback;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_customer_home, container, false);

        newBooking = view.findViewById(R.id.newBooking);
        viewBooking = view.findViewById(R.id.viewBooking);
        provideFeedback = view.findViewById(R.id.provideFeedback);

        newBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), NewBookingActivity.class));
            }
        });

        viewBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), UserViewBookingActivity.class));
            }
        });

        provideFeedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), FeedbackActivity.class));
            }
        });


        return view;
    }
}