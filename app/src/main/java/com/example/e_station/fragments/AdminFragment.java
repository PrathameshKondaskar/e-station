package com.example.e_station.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.e_station.R;
import com.example.e_station.activities.admin.AddManagerActivity;
import com.example.e_station.activities.admin.AddStationActivity;
import com.example.e_station.activities.admin.TotalBookingActivity;


public class AdminFragment extends Fragment {

    CardView addStation, addManager, totalBooking;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_admin, container, false);

        addStation = view.findViewById(R.id.addStation);
        addManager = view.findViewById(R.id.addManager);
        totalBooking = view.findViewById(R.id.totalBooking);

        addStation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddStationActivity.class));
            }
        });

        addManager.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddManagerActivity.class));
            }
        });

        totalBooking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), TotalBookingActivity.class));
            }
        });


        return view;
    }
}