package com.example.e_station.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;


import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.models.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.installations.FirebaseInstallations;

public class SignupTabFragment extends Fragment {

    //Views
    private TextInputEditText email , password  , mobile , name;
    private Button signup ;
    private ProgressDialog progressDialog;

    // Initialize Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    UserInfoModel userInfo;
    UserSharedPreference userSharedPreference;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view = (ViewGroup) inflater.inflate(R.layout.fragment_signup_tab, container, false);

        userSharedPreference = new UserSharedPreference(getContext());
        progressDialog= new ProgressDialog(getContext());
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        mobile = view.findViewById(R.id.mobile);
        name = view.findViewById(R.id.name);
        signup = view.findViewById(R.id.buttonSignup);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    registerUser();
            }
        });


        return view;

    }

    public void registerUser(){

        final String userEmail = email.getText().toString().trim();
        final String userPassword = password.getText().toString().trim();
        final String userMobile = mobile.getText().toString();
        final String userName = name.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (userName.equals("")) {
            shouldCancelSignUp = true;
            focusView = name;
            name.setError("name is a required field");
        }
        if (userEmail.equals("")) {
            shouldCancelSignUp = true;
            focusView = email;
            email.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            shouldCancelSignUp = true;
            focusView = email;
            email.setError("email is not valid");
        }

        if (userPassword.equals("")) {
            shouldCancelSignUp = true;
            focusView = password;
            password.setError("Password is a required field");
        }

        if (userPassword.length() < 6) {
            shouldCancelSignUp = true;
            focusView = password;
            password.setError("Password must be greater than 6 letters");
        }

        if (userMobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a required field");
        }
        if (!userMobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a invalid");

        }
        if (userMobile.length() != 10 || userMobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. must be of 10 digit");
        }


        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            firebaseLogin(userEmail,userPassword,userMobile,userName);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // Check if user is signed in (non-null) and update UI accordingly.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){

        }
    }

    public void firebaseLogin(String email ,String password ,String mobile,String name){

        progressDialog.setMessage("Sign Up...");
        progressDialog.show();
        progressDialog.setCancelable(false);


        mAuth.createUserWithEmailAndPassword(email,password)
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        if(task.isSuccessful())
                        {
                            String userId = mAuth.getUid();
                            userInfo = new UserInfoModel(name,email,password,mobile,"","1","","",userId,"","","","");
                            userSharedPreference.setUserId(userId);

                            mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO).document(userId).
                            set(userInfo)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if(task.isSuccessful()) {
                                                progressDialog.dismiss();

                                                FirebaseInstallations.getInstance().getId()
                                                        .addOnCompleteListener(new OnCompleteListener<String>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<String> task) {

                                                                if (!task.isSuccessful()) {
                                                                    Log.w("ERR", "getInstanceId failed", task.getException());
                                                                    return;
                                                                }

                                                                // Get new Instance ID token
                                                                String token = task.getResult();

                                                                Toast.makeText(getContext(), token, Toast.LENGTH_SHORT).show();
                                                                if(userSharedPreference.getToken()==null)
                                                                {

                                                                    userSharedPreference.setToken(token);

                                                                }

                                                            }
                                                        });

                                                Toast.makeText(getContext(), "Signup Successful", Toast.LENGTH_LONG).show();
                                               /* Intent intent = new Intent(getContext(), AdminMainActivity.class);
                                                intent.putExtra("IS_FROM","Login");
                                                startActivity(intent);
                                                getActivity().finish();*/



                                            }else {
                                                progressDialog.dismiss();
                                                Log.d("ERR",task.getException().toString());
                                                Toast.makeText(getContext(), "data not added", Toast.LENGTH_LONG).show();

                                            }
                                        }
                                    });


                        }
                        else {
                            progressDialog.dismiss();
                            if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                        }

                    }
                });

    }
}