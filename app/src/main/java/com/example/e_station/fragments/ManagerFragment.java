package com.example.e_station.fragments;

import android.content.Intent;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.e_station.R;
import com.example.e_station.activities.manager.AddVehicleActivity;
import com.example.e_station.activities.manager.CompleteOrderActivity;
import com.example.e_station.activities.manager.PendingOrderActivity;
import com.example.e_station.activities.user.FeedbackActivity;
import com.example.e_station.activities.user.NewBookingActivity;
import com.example.e_station.activities.user.UserViewBookingActivity;

public class ManagerFragment extends Fragment {
    CardView addVehicle, pendingOrder, completeOrder;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_manager, container, false);

        addVehicle = view.findViewById(R.id.addVehicle);
        pendingOrder = view.findViewById(R.id.pendingOrder);
        completeOrder = view.findViewById(R.id.completeOrder);

        addVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), AddVehicleActivity.class));
            }
        });

        pendingOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), PendingOrderActivity.class));
            }
        });

        completeOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), CompleteOrderActivity.class));
            }
        });


        return view;
    }
}