package com.example.e_station.fragments;


import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.example.e_station.R;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.e_station.UserSharedPreference;
import com.example.e_station.activities.MainActivity;
import com.example.e_station.models.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class LoginTabFragment extends Fragment {

    TextInputEditText email , password;
    TextInputLayout outlineEmail,outlinePassword;
    Button login;
    TextView forgotPassword;
    float v =0;
    private ProgressDialog progressDialog;

    String pEmail ,pPassword;

    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    String role="";
    String eStationName ="",eStationId="",eStationAddress="";
    UserSharedPreference userSharedPreference;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ViewGroup view =(ViewGroup) inflater.inflate(R.layout.fragment_login_tab, container, false);
        inflater.getContext().setTheme(R.style.AppThemeMaterial);


        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();

        userSharedPreference = new UserSharedPreference(getContext());
        progressDialog= new ProgressDialog(getContext());
        email = view.findViewById(R.id.email);
        password = view.findViewById(R.id.password);
        login = view.findViewById(R.id.buttonlogin);
        forgotPassword = view.findViewById(R.id.tvForgotPassword);
        outlineEmail = view.findViewById(R.id.outlineEmail);
        outlinePassword = view.findViewById(R.id.outlinePassword);



        login.setTranslationX(800);
        forgotPassword.setTranslationX(800);

        login.setAlpha(v);
        forgotPassword.setAlpha(v);

        login.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(700).start();
        forgotPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(500).start();

        TranslateAnimation animation1 = new TranslateAnimation(1500.0f, 0.0f, 0.0f, 0.0f); // new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation1.setDuration(800); // animation duration
        animation1.setFillAfter(true);
        animation1.setStartOffset(300);
        outlineEmail .startAnimation(animation1);

        TranslateAnimation animation2 = new TranslateAnimation(1500.0f, 0.0f, 0.0f, 0.0f); // new TranslateAnimation(xFrom,xTo, yFrom,yTo)
        animation2.setDuration(800); // animation duration
        animation2.setFillAfter(true);
        animation2.setStartOffset(500);
        outlinePassword .startAnimation(animation2);

        login.setOnClickListener(view1 -> userLogin());


        forgotPassword.setOnClickListener(view12 -> {
           /* Intent intent = new Intent(getContext(), ForgotPasswordActivity.class);
            startActivity(intent);*/
        });


        return view;
    }

    public void userLogin() {


        pEmail = email.getText().toString().trim();
        pPassword = password.getText().toString().trim();
      /*  pEmail = "admin";
        pPassword ="admin";*/
        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (pEmail.equals("")) {
            shouldCancelSignUp = true;
            focusView = email;
            email.setError("Email is a required field");
        }
        if (pPassword.equals("")) {
            shouldCancelSignUp = true;
            focusView = password;
            password.setError("Password is a required field");
        }
        if(pEmail.equals("admin") && pPassword.equals("admin"))
        {
            Intent i = new Intent(getContext(), MainActivity.class);
            i.putExtra("IS_FROM","admin");
            startActivity(i);
        }else
        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }


        else {
            progressDialog.setMessage("Log In...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            mAuth.signInWithEmailAndPassword(pEmail, pPassword)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {

                            if (task.isSuccessful()) {
                                getUserData();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(getContext(), task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(user!=null){

                Toast.makeText(getContext(), "Login Successfull.", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getContext(), MainActivity.class);
                intent.putExtra("IS_FROM", "Login");
                startActivity(intent);
                getActivity().finish();

        }
    }

    public void getUserData()
    {

        mFirestore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO).get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {

                        if(task.isSuccessful()) {


                            for (QueryDocumentSnapshot documentSnapshot : task.getResult()) {

                                if (pEmail.matches((String) documentSnapshot.getData().get("email")))
                                {
                                    role = (String) documentSnapshot.getData().get("type");
                                    eStationId = (String) documentSnapshot.getData().get("estationId");
                                    eStationName = (String) documentSnapshot.getData().get("estationName");
                                    eStationAddress = (String) documentSnapshot.getData().get("address");

                                    userSharedPreference.setEstationId(eStationId);
                                    userSharedPreference.setEstationName(eStationName);
                                    userSharedPreference.setEstationAddress(eStationAddress);
                                    userSharedPreference.setUserName((String) documentSnapshot.getData().get("name"));
                                    userSharedPreference.setMobile((String) documentSnapshot.getData().get("mobile"));

                                    userSharedPreference.setRole(role);


                                        progressDialog.dismiss();


                                        Toast.makeText(getContext(), "Login Successfull.", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(getContext(), MainActivity.class);
                                        intent.putExtra("IS_FROM","Login");
                                        startActivity(intent);
                                        getActivity().finish();

                                }

                            }
                        }
                    }
                });
    }
}