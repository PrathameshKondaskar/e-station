package com.example.e_station.models;

public class UserInfoModel {

    String name="", email="", password="", mobile="",address,type="", imageUri="",carImageUri="";
    String userId="",estationId="",estationName="",vehicleName="",vehicleModel="";
    public static final String FIREBASE_COLLECTION_USERINFO = "UserInfo";

    public UserInfoModel(String name, String email, String password, String mobile, String address, String type, String imageUri, String carImageUri, String userId, String estationId, String estationName, String vehicleName, String vehicleModel) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.mobile = mobile;
        this.address = address;
        this.type = type;
        this.imageUri = imageUri;
        this.carImageUri = carImageUri;
        this.userId = userId;
        this.estationId = estationId;
        this.estationName = estationName;
        this.vehicleName = vehicleName;
        this.vehicleModel = vehicleModel;
    }

    public UserInfoModel() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImageUri() {
        return imageUri;
    }

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEstationId() {
        return estationId;
    }

    public void setEstationId(String estationId) {
        this.estationId = estationId;
    }

    public String getEstationName() {
        return estationName;
    }

    public void setEstationName(String estationName) {
        this.estationName = estationName;
    }

    public static String getFirebaseCollectionUserinfo() {
        return FIREBASE_COLLECTION_USERINFO;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getCarImageUri() {
        return carImageUri;
    }

    public void setCarImageUri(String carImageUri) {
        this.carImageUri = carImageUri;
    }
}
