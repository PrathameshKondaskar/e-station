package com.example.e_station.models;

public class BookingModel {
    String docId="";
    String userId ="";
    String userName="";
    String mobile="";
    String orderId="";
    String eStationId ="";
    String eStationName ="";
    String eStationAddress="";
    String date ="";
    String timeSlot ="";
    String rate="";
    String paymentType="";
    String status ="";
    String managerId="";
    String vehicleName ="";
    String vehicleModel="";
    public static final String FIREBASE_COLLECTION_BOOKINGMODEL= "BookingTable";

    public BookingModel(String docId,String userId, String userName, String mobile, String orderId, String eStationId, String eStationName, String eStationAddress, String date, String timeSlot, String rate, String paymentType, String status, String managerId, String vehicleName, String vehicleModel) {
        this.docId = docId;
        this.userId = userId;
        this.userName = userName;
        this.mobile = mobile;
        this.orderId = orderId;
        this.eStationId = eStationId;
        this.eStationName = eStationName;
        this.eStationAddress = eStationAddress;
        this.date = date;
        this.timeSlot = timeSlot;
        this.rate = rate;
        this.paymentType = paymentType;
        this.status = status;
        this.managerId = managerId;
        this.vehicleName = vehicleName;
        this.vehicleModel = vehicleModel;
    }

    public BookingModel() {
    }

    public String geteStationId() {
        return eStationId;
    }

    public void seteStationId(String eStationId) {
        this.eStationId = eStationId;
    }

    public String geteStationName() {
        return eStationName;
    }

    public void seteStationName(String eStationName) {
        this.eStationName = eStationName;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTimeSlot() {
        return timeSlot;
    }

    public void setTimeSlot(String timeSlot) {
        this.timeSlot = timeSlot;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getManagerId() {
        return managerId;
    }

    public void setManagerId(String managerId) {
        this.managerId = managerId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String geteStationAddress() {
        return eStationAddress;
    }

    public void seteStationAddress(String eStationAddress) {
        this.eStationAddress = eStationAddress;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public String getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(String vehicleModel) {
        this.vehicleModel = vehicleModel;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getDocId() {
        return docId;
    }

    public void setDocId(String docId) {
        this.docId = docId;
    }
}
