package com.example.e_station.models;

import java.io.Serializable;
import java.util.ArrayList;

public class PowerStationModel implements Serializable {
    String id, name,address,lat,lang;
    ArrayList<Slots> slotsArrayList;
    public static final String FIREBASE_COLLECTION_POWERSTATION = "PowerStations";

    public PowerStationModel() {
    }

    public PowerStationModel(String id, String name, String address, String lat, String lang, ArrayList<Slots> slotsArrayList) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.lat = lat;
        this.lang = lang;
        this.slotsArrayList = slotsArrayList;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public ArrayList<Slots> getSlotsArrayList() {
        return slotsArrayList;
    }

    public void setSlotsArrayList(ArrayList<Slots> slotsArrayList) {
        this.slotsArrayList = slotsArrayList;
    }

    public static class Slots{
        String from ,to ,rate;

        public Slots() {
        }

        public Slots(String from, String to,String rate) {

            this.from = from;
            this.to = to;
            this.rate = rate;
        }

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public String getRate() {
            return rate;
        }

        public void setRate(String rate) {
            this.rate = rate;
        }




    }
}
