package com.example.e_station.models;

import android.net.Uri;

public class ImageUriModel {
    Uri imageUri;
    String imageType;

    public ImageUriModel() {
    }

    public ImageUriModel(Uri imageUri, String imageType) {
        this.imageUri = imageUri;
        this.imageType = imageType;
    }

    public Uri getImageUri() {
        return imageUri;
    }

    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    public String getImageType() {
        return imageType;
    }

    public void setImageType(String imageType) {
        this.imageType = imageType;
    }
}
