package com.example.e_station.activities.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.example.e_station.R;
import com.example.e_station.models.PowerStationModel;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity implements GoogleMap.OnMarkerClickListener {

    private GoogleMap mMap;
    private FusedLocationProviderClient fusedLocationClient;
    SupportMapFragment mapFragment;
    public ArrayList<PowerStationModel> powerStationsList=new ArrayList<>();

    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        progressDialog= new ProgressDialog(this);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

         mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        if (ActivityCompat.checkSelfPermission(MapsActivity.this, Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {

            getCurrentLocation();
        }else{
            ActivityCompat.requestPermissions(MapsActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},101);
        }
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

//        mapFragment.getMapAsync(this);
    }

   /* public void getPowerStationList(){
        powerStationsList.add(new PowerStations("Power Station 1",18.917155,72.8222106));
        powerStationsList.add(new PowerStations("Power Station 2",18.9138563,72.8295852));
        powerStationsList.add(new PowerStations("Power Station 3",18.9160386,72.8168783));

        drawMarkerOnMap();
    }*/

    public void drawMarkerOnMap(){
        for (PowerStationModel powerStations: powerStationsList) {
            LatLng latLng = new LatLng(Double.parseDouble(powerStations.getLat()),Double.parseDouble(powerStations.getLang()));
            MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(powerStations.getName())
                    .icon(BitmapFromVector(getApplicationContext(),R.drawable.estationmap));;
            mMap.addMarker(markerOptions);

        }
    }

    private void getCurrentLocation() {

        Task<Location> task = fusedLocationClient.getLastLocation();

    task.addOnSuccessListener(new OnSuccessListener<Location>() {
        @Override
        public void onSuccess(Location location) {

            if(location !=null){

                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(@NonNull GoogleMap googleMap) {
                        mMap = googleMap;

                        LatLng latLng = new LatLng(location.getLatitude(),location.getLongitude());

                        MarkerOptions markerOptions = new MarkerOptions()
                                                         .position(latLng)
                                                         .title("I am Here")
                                                         .icon(BitmapFromVector(getApplicationContext(),R.drawable.carmapicon));

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng,15));
                        mMap.addMarker(markerOptions);

                        mMap.setOnMarkerClickListener((GoogleMap.OnMarkerClickListener)MapsActivity.this);

                        getPowerStationList();
                    }
                });

            }

        }
    }).addOnFailureListener(new OnFailureListener() {
        @Override
        public void onFailure(@NonNull Exception e) {
            Log.d("TAG", "onFailure: "+e.getMessage().toString());
        }
    });
    }

    private BitmapDescriptor BitmapFromVector(Context context, int vectorResId) {
        // below line is use to generate a drawable.
        Drawable vectorDrawable = ContextCompat.getDrawable(context, vectorResId);

        // below line is use to set bounds to our vector drawable.
        vectorDrawable.setBounds(0, 0, vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight());

        // below line is use to create a bitmap for our
        // drawable which we have added.
        Bitmap bitmap = Bitmap.createBitmap(vectorDrawable.getIntrinsicWidth(), vectorDrawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);

        // below line is use to add bitmap in our canvas.
        Canvas canvas = new Canvas(bitmap);

        // below line is use to draw our
        // vector drawable in canvas.
        vectorDrawable.draw(canvas);

        // after generating our bitmap we are returning our bitmap.
        return BitmapDescriptorFactory.fromBitmap(bitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 101) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getCurrentLocation();
            }
        }
    }

    @Override
    public boolean onMarkerClick(@NonNull Marker marker) {

        String name= marker.getTitle();

        for(int i = 0;i< powerStationsList.size();i++) {
            if (name.equalsIgnoreCase(powerStationsList.get(i).getName())) {
                //write your code here
                showDialog(powerStationsList.get(i));
                Toast.makeText(MapsActivity.this, "power"+powerStationsList.get(i).getName(), Toast.LENGTH_SHORT).show();
            }
        }
        return false;
    }

    public void showDialog(PowerStationModel powerStationModel)  {

        AlertDialog.Builder builder = new AlertDialog.Builder(MapsActivity.this,R.style.MyDialogTheme);

        builder.setMessage("Do you want to book at "+powerStationModel.getName()+" ?" );


        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                Intent intent = getIntent();
                Gson gson = new Gson();
                String myJson = gson.toJson(powerStationModel);
                intent.putExtra("PowerStation",myJson);
                setResult(100,intent);
                finish();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener(){
            @Override
            public void onClick(DialogInterface dialog, int which){
                dialog.dismiss();
            }
        });
        builder.show();
    }

    public void getPowerStationList(){
        progressDialog.setMessage("Loading Stations...");
        progressDialog.show();
        progressDialog.setCancelable(false);

        mFireStore.collection(PowerStationModel.FIREBASE_COLLECTION_POWERSTATION)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot querySnapshot) {
                        if(!querySnapshot.isEmpty()) {

                            progressDialog.dismiss();
                            List<PowerStationModel> types = querySnapshot.toObjects(PowerStationModel.class);
                            powerStationsList.addAll(types);
                            drawMarkerOnMap();
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressDialog.dismiss();
                Toast.makeText(MapsActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
   /* @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }*/
}