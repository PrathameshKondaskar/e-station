package com.example.e_station.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.Menu;

import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.activities.authentication.LoginActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity implements  NavigationView.OnNavigationItemSelectedListener{

    private AppBarConfiguration mAppBarConfiguration;
    public static String isFrom="";
    UserSharedPreference userSharedPreference ;
    String role="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        isFrom = intent.getStringExtra("IS_FROM");
        userSharedPreference = new UserSharedPreference(this);
        role = userSharedPreference.getRole();
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        if(!userSharedPreference.getRole().equalsIgnoreCase("")) {
            if (userSharedPreference.getRole().equalsIgnoreCase("1")) {
                mAppBarConfiguration = new AppBarConfiguration.Builder(
                        R.id.nav_home,R.id.nav_home_customer, R.id.nav_profile)
                        .setOpenableLayout(drawer)
                        .build();
            } else if (userSharedPreference.getRole().equalsIgnoreCase("2")) {
                mAppBarConfiguration = new AppBarConfiguration.Builder(
                        R.id.nav_home,R.id.nav_manager_complete, R.id.nav_manager_pending, R.id.nav_home_manager)
                        .setOpenableLayout(drawer)
                        .build();
            }
        }else{
            mAppBarConfiguration = new AppBarConfiguration.Builder(
                     R.id.nav_home,R.id.nav_home_admin)
                    .setOpenableLayout(drawer)
                    .build();
        }
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);

        Menu menu = navigationView.getMenu();
        if(isFrom.equalsIgnoreCase("Login")) {
            //            menu.findItem(R.id.nav_prop).setVisible(false);
            if(userSharedPreference.getRole().equalsIgnoreCase("1")) {
                menu.findItem(R.id.nav_home_customer).setVisible(true);
                menu.findItem(R.id.nav_home).setVisible(true);
                menu.findItem(R.id.nav_profile).setVisible(true);
                menu.findItem(R.id.nav_home).setVisible(false);

                navController.popBackStack(R.id.nav_home, true);
                navController.navigate(R.id.nav_home_customer, null);
            }else{
                menu.findItem(R.id.nav_home).setVisible(false);
                menu.findItem(R.id.nav_manager_pending).setVisible(true);
                menu.findItem(R.id.nav_manager_complete).setVisible(true);
                menu.findItem(R.id.nav_home_manager).setVisible(true);

                navController.popBackStack(R.id.nav_home, true);
                navController.navigate(R.id.nav_home_manager, null);

            }

        }else{
            menu.findItem(R.id.nav_admin_addmgr).setVisible(true);
            menu.findItem(R.id.nav_home_admin).setVisible(true);
            menu.findItem(R.id.nav_admin_totalBooking).setVisible(true);
            navController.popBackStack(R.id.nav_home, true);
            navController.navigate(R.id.nav_home_admin,null);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_content_main);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {


        int id = item.getItemId();


        if (id == R.id.logOut) {
            FirebaseAuth.getInstance().signOut();
            userSharedPreference.clearSharedPreferences();
            startActivity(new Intent(MainActivity.this, LoginActivity.class));
            finish();
        }

        return super.onOptionsItemSelected(item);
    }
}