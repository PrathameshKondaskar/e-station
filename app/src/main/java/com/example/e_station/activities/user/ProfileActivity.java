package com.example.e_station.activities.user;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.models.ImageUriModel;
import com.example.e_station.models.UserInfoModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProfileActivity extends AppCompatActivity {

    int flag = 0;
    int carflag =0;
    String imgurl;
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int CAR_IMAGE_REQUEST = 2;
    private ArrayList<ImageUriModel> imageUriModels = new ArrayList<>();
    private ArrayList<ImageUriModel> firebaseImageUriModels = new ArrayList<>();
    private Uri mCarUri =null;


    // Initialize Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    UserInfoModel userInfo;
    UserSharedPreference userSharedPreference;
    private StorageReference mStorageRef;
    private ArrayList<UserInfoModel> userInfoModelList = new ArrayList<>() ;

    //Views
    private ImageView imageView,backButton,carlogo;
    private Button submit;
    private TextInputEditText email , vehicleName, vehileModel  , mobile , name;
    private ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        initView();
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updateProfile();
            }
        });

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser(PICK_IMAGE_REQUEST);
            }
        });
        carlogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser(CAR_IMAGE_REQUEST);
            }
        });

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initView() {

        carlogo = findViewById(R.id.carlogo);
        imageView = findViewById(R.id.imageProfile);
        submit = findViewById(R.id.buttonSubmit);
        backButton = findViewById(R.id.backButton);
        userSharedPreference = new UserSharedPreference(this);
        progressDialog= new ProgressDialog(this);
        email = findViewById(R.id.email);
        vehicleName = findViewById(R.id.vehicleName);
        vehileModel = findViewById(R.id.vehicleModel);
        mobile = findViewById(R.id.mobile);
        name = findViewById(R.id.name);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();
        mStorageRef = FirebaseStorage.getInstance().getReference();

        getProfileData();
    }

    public void getProfileData(){
        progressDialog.setMessage("Loading Profile...");
        progressDialog.show();
        progressDialog.setCancelable(false);


        mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                .whereEqualTo("userId",mAuth.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot querySnapshot) {
                        if(!querySnapshot.isEmpty()) {

                            progressDialog.dismiss();
                            List<UserInfoModel> types = querySnapshot.toObjects(UserInfoModel.class);
                            userInfoModelList.addAll(types);

                            UserInfoModel userInfoModel = userInfoModelList.get(0);

                            email.setText(userInfoModel.getEmail());
                            vehicleName.setText(userInfoModel.getVehicleName());
                            vehileModel.setText(userInfoModel.getVehicleModel());
                            mobile.setText(userInfoModel.getMobile());
                            name.setText(userInfoModel.getName());

                            if(!userInfoModel.getImageUri().equalsIgnoreCase("")) {
                                Glide.with(ProfileActivity.this)
                                        .load(userInfoModel.getImageUri())
                                        .into(imageView);
                            }

                            if(!userInfoModel.getCarImageUri().equalsIgnoreCase("")) {
                                Glide.with(ProfileActivity.this)
                                        .load(userInfoModel.getCarImageUri())
                                        .into(carlogo);
                            }
                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, ""+e.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }

    public void updateProfile(){

        final String userEmail = email.getText().toString().trim();
        final String userVehicleName = vehicleName.getText().toString();
        final String userVehicleModel = vehileModel.getText().toString();
        final String userMobile = mobile.getText().toString();
        final String userName = name.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (userName.equals("")) {
            shouldCancelSignUp = true;
            focusView = name;
            name.setError("name is a required field");
        }
        if (userEmail.equals("")) {
            shouldCancelSignUp = true;
            focusView = email;
            email.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            shouldCancelSignUp = true;
            focusView = email;
            email.setError("email is not valid");
        }


        if (userMobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a required field");
        }
        if (!userMobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || userMobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. must be of 10 digit");
        }
        if (userVehicleName.equals("")) {
            shouldCancelSignUp = true;
            focusView = vehicleName;
            vehicleName.setError("email is a required field");
        }
        if (userVehicleModel.equals("")) {
            shouldCancelSignUp = true;
            focusView = vehileModel;
            vehileModel.setError("email is a required field");
        }


        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {

            progressDialog.setMessage("Updating Profile...");
            progressDialog.show();
            progressDialog.setCancelable(false);


            if (imageUriModels != null && imageUriModels.size()>0) {
                for(int i =0 ; i< imageUriModels.size() ;i++){
                    final String path = System.currentTimeMillis() + "." + getExtenssion(imageUriModels.get(i).getImageUri());
                    StorageReference fileRef=null;
                    if(imageUriModels.get(i).getImageType().equalsIgnoreCase("1")){
                       fileRef   = mStorageRef.child(" Profile Pictures").child(path);
                    }else{
                        fileRef   = mStorageRef.child("Car images").child(path);
                    }

                    StorageReference finalFileRef = fileRef;
                    int finalI = i;
                    finalFileRef.putFile(imageUriModels.get(i).getImageUri()).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                        @Override
                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                            if (!task.isSuccessful()) {
                                throw task.getException();
                            }
                            Log.d("TAG", "then: "+finalFileRef.getDownloadUrl().toString());
                            return finalFileRef.getDownloadUrl();
                        }
                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                        @Override
                        public void onComplete(@NonNull Task<Uri> task) {
                            Uri downUri = task.getResult();
                            if(imageUriModels.get(finalI).getImageType().equalsIgnoreCase("1")){
                                firebaseImageUriModels.add(new ImageUriModel(task.getResult(),"1"));
                                Log.d("TAG", "onComplete afterupload: "+task.getResult().toString());
                            }else{
                                Log.d("TAG", "onComplete afterupload: "+task.getResult().toString());
                                firebaseImageUriModels.add(new ImageUriModel(task.getResult(),"2"));
                            }

                            Log.d("TAG", "i : "+finalI);
                            if(firebaseImageUriModels.size() == imageUriModels.size()){

                                Log.d("TAG", "onComplete: map");
                                Map<String, Object> map = new HashMap<>();
                                map.put("name", userName);
                                map.put("vehicleName", userVehicleName);
                                map.put("vehicleModel",userVehicleModel);
                                map.put("email", userEmail);
                                map.put("mobile", userMobile);
                                for(int j = 0; j < firebaseImageUriModels.size() ; j++){
                                    if(firebaseImageUriModels.get(j).getImageType().equalsIgnoreCase("1")){
                                        map.put("imageUri", firebaseImageUriModels.get(j).getImageUri().toString());
                                    }else{
                                        map.put("carImageUri", firebaseImageUriModels.get(j).getImageUri().toString());
                                    }
                                }

                                mFireStore
                                        .collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                                        .document(mAuth.getUid())
                                        .update(map)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                progressDialog.dismiss();
                                                Toast.makeText(ProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                                                imageUriModels.clear();
                                                firebaseImageUriModels.clear();
                                            }
                                        });

                            }
                        }
                    })

                    .addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            progressDialog.dismiss();
                            Log.d("TAG", "onFailure: "+e.getMessage());
                        }
                    });

                }


            }else
            {

                Map<String, Object> map = new HashMap<>();
                map.put("name", userName);
                map.put("vehicleName", userVehicleName);
                map.put("vehicleModel",userVehicleModel);
                map.put("email", userEmail);
                map.put("mobile", userMobile);

                mFireStore
                        .collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                        .document(mAuth.getUid())
                        .update(map)
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                progressDialog.dismiss();
                                Toast.makeText(ProfileActivity.this, "Profile Updated Successfully", Toast.LENGTH_SHORT).show();
                            }
                        });
            }
        }


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            ImageUriModel imageUriModel = new ImageUriModel();
            imageUriModel.setImageUri(data.getData());
            imageUriModel.setImageType("1");
            imageUriModels.add(imageUriModel);
            Log.d("path", imageUriModel.getImageUri().toString());
            imageView.setImageURI(imageUriModel.getImageUri());
            imageView.buildDrawingCache();
            flag = 1;

        }else if(requestCode == CAR_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            ImageUriModel imageUriModel = new ImageUriModel();
            imageUriModel.setImageUri(data.getData());
            imageUriModel.setImageType("2");
            imageUriModels.add(imageUriModel);
            Log.d("path", imageUriModel.getImageUri().toString());
            carlogo.setImageURI(imageUriModel.getImageUri());
            carlogo.buildDrawingCache();
            carflag = 1;
        }
    }

    private String getExtenssion(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

    }
    private void openFileChooser(int imageRequest) {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, imageRequest);

    }


}