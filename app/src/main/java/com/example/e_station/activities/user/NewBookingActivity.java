package com.example.e_station.activities.user;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.Toolbar;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.adapter.CustomSpinnerAdapter;
import com.example.e_station.databinding.ActivityNewBookingBinding;
import com.example.e_station.models.BookingModel;
import com.example.e_station.models.PowerStationModel;
import com.example.e_station.models.UserInfoModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.Gson;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class NewBookingActivity extends AppCompatActivity {

    ActivityNewBookingBinding activityNewBookingBinding;
    String eStationName="",time="",rate="",paymentType="",eStationId ="",managerId="",estationAddress="";
    String vehicleName ="",vehicleModel ="";
    private int mYear, mMonth, mDay, mHour, mMinute;
    Date selecteddate;
    String formattedDate="",currentDate;
    Calendar c1;
    SimpleDateFormat df;
    String slot ="";
    private PowerStationModel powerStationModel= new PowerStationModel();
    ArrayList<PowerStationModel.Slots> slots = new ArrayList<>();
    private ArrayList<String> timeSoltList = new ArrayList<>();
    private ArrayList<UserInfoModel> userInfoModelList = new ArrayList<>() ;
    AppCompatRadioButton radioButton;

    // Initialize Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private ProgressDialog progressDialog;

    UserSharedPreference userSharedPreference;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityNewBookingBinding = ActivityNewBookingBinding.inflate(getLayoutInflater());
        setContentView(activityNewBookingBinding.getRoot());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        progressDialog = new ProgressDialog(this);
        userSharedPreference = new UserSharedPreference(this);
        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        getUserVehicleDetail();

        df = new SimpleDateFormat("dd/MM/yyyy");
        c1 = Calendar.getInstance();
        selecteddate = c1.getTime();
        currentDate= df.format(c1.getTime());

        activityNewBookingBinding.selectLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(NewBookingActivity.this, MapsActivity.class);
                startActivityForResult(intent ,100);


            }
        });

        activityNewBookingBinding.selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);
                DatePickerDialog datePickerDialog = new DatePickerDialog(NewBookingActivity.this,
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {


                                Calendar td = Calendar.getInstance();
                                c.add(Calendar.DATE,1);
                                Date today = c.getTime();

                                c1.set(Calendar.YEAR, year);
                                c1.set(Calendar.MONTH, monthOfYear);
                                c1.set(Calendar.DAY_OF_MONTH, dayOfMonth);


                                df = new SimpleDateFormat("dd/MM/yyyy");


                                formattedDate= df.format(c1.getTime());
                                activityNewBookingBinding.selectDate.setText(formattedDate);

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.show();

            }
        });


        activityNewBookingBinding.spotSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                slot = timeSoltList.get(i);
                rate = slots.get(i).getRate();
                time = slots.get(i).getFrom()+"-"+slots.get(i).getTo();
            }
        });

        activityNewBookingBinding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedType = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedType);
                paymentType = radioButton.getText().toString();
            }
        });
        activityNewBookingBinding.buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                boolean shouldCancelSignUp = false;
                View focusView = null;

                if (eStationName.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = activityNewBookingBinding.selectLocation;
                    activityNewBookingBinding.selectLocation.setError("Please select the location");
                }else{
                    shouldCancelSignUp = false;
                    activityNewBookingBinding.selectLocation.setError(null);
                    activityNewBookingBinding.selectLocation.clearFocus();
                }

                if (formattedDate.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = activityNewBookingBinding.selectDate;
                    activityNewBookingBinding.selectDate.setError("Please select the date");
                }else
                {
                    shouldCancelSignUp = false;
                    activityNewBookingBinding.selectDate.setError(null);
                    activityNewBookingBinding.selectDate.clearFocus();
                }
                if (time.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = activityNewBookingBinding.spotSpinner;
                    activityNewBookingBinding.spotSpinner.setError("Please select the time slot");
                }else{
                    shouldCancelSignUp = false;
                    activityNewBookingBinding.spotSpinner.setError(null);
                    activityNewBookingBinding.spotSpinner.clearFocus();
                }
                if (paymentType.equals("")) {
                    shouldCancelSignUp = true;
                    focusView = activityNewBookingBinding.radioGroup;
                    Toast.makeText(NewBookingActivity.this, "Please select the payment option", Toast.LENGTH_SHORT).show();
                }else{
                    shouldCancelSignUp = false;
                }
                if (shouldCancelSignUp) {
                    // There was an error; don't attempt login and focus the first
                    // form field with an error.
                    focusView.requestFocus();
                }else {

                    progressDialog.show();
                    progressDialog.setMessage("Booking");

                    BookingModel bookingModel = new BookingModel("",mAuth.getUid(), userSharedPreference.getUserName(), userSharedPreference.getMobile(),randomNo(),eStationId,eStationName,estationAddress,formattedDate,time,rate,paymentType,"0",managerId,vehicleName,vehicleModel);
                    saveBookingToFirebase(bookingModel);
                }


            }
        });
    }

    private void saveBookingToFirebase(BookingModel bookingModel) {
        mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL).add(bookingModel)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful())
                        {
                            if(progressDialog!=null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
//                            Toast.makeText(NewBookingActivity.this, "Power Station  added", Toast.LENGTH_SHORT).show();
//                            cleaAllValues();
                            showDialog("Booking Successful",2);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(NewBookingActivity.this, "Property not added", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("TAG", "onFailure: "+e.getMessage());
                    }
                });
    }

    public void showDialog (String msg , int type){
        SweetAlertDialog pDialog = new SweetAlertDialog(NewBookingActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                finish();
                sDialog.cancel();
            }
        });
        pDialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode ==100) {
            Gson gson = new Gson();
            powerStationModel = gson.fromJson(data.getStringExtra("PowerStation"), PowerStationModel.class);
            activityNewBookingBinding.selectLocation.setText(powerStationModel.getName());
            eStationName = powerStationModel.getName();
            eStationId = powerStationModel.getId();
            estationAddress = powerStationModel.getAddress();
            if (powerStationModel.getSlotsArrayList().size() > 0) {
                slots = powerStationModel.getSlotsArrayList();
                getManagerId(powerStationModel);
            }
            if (slots.size() > 0) {
                for (int i = 0; i < slots.size(); i++) {

                    timeSoltList.add(slots.get(i).getFrom() + "-" + slots.get(i).getTo() + "    Rs." + slots.get(i).getRate());
                }
                CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(getApplicationContext(), android.R.layout.simple_list_item_1, timeSoltList);
                activityNewBookingBinding.spotSpinner.setAdapter(customAdapter);
                activityNewBookingBinding.spotSpinner.setText(timeSoltList.get(0), false);
                time = slots.get(0).getFrom()+"-"+slots.get(0).getTo();
                rate = slots.get(0).getRate();
            }

            if(!eStationName.equalsIgnoreCase("")){
                activityNewBookingBinding.selectLocation.setError(null);
                activityNewBookingBinding.selectLocation.clearFocus();
            }

            if(!time.equalsIgnoreCase("")){
                activityNewBookingBinding.spotSpinner.setError(null);
                activityNewBookingBinding.spotSpinner.clearFocus();
            }

        }
    }
    public void getUserVehicleDetail(){
        mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                .whereEqualTo("userId",mAuth.getUid())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        userInfoModelList = new ArrayList<>();
                        List<UserInfoModel> types = queryDocumentSnapshots.toObjects(UserInfoModel.class);
                        userInfoModelList.addAll(types);
                        UserInfoModel userInfoModel = userInfoModelList.get(0);
                        vehicleName = userInfoModel.getVehicleName();
                        vehicleModel = userInfoModel.getVehicleModel();
                    }
                });
    }
    public void getManagerId(PowerStationModel powerStationModel){
        mFireStore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO)
                .whereEqualTo("estationId",powerStationModel.getId())
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        userInfoModelList = new ArrayList<>();
                        List<UserInfoModel> types = queryDocumentSnapshots.toObjects(UserInfoModel.class);
                        userInfoModelList.addAll(types);
                        UserInfoModel userInfoModel = userInfoModelList.get(0);
                        managerId = userInfoModel.getUserId();
                    }
                });

    }

    public String randomNo() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return "O-"+sb1.toString();
    }

}