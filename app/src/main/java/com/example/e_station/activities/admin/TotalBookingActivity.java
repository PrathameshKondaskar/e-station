package com.example.e_station.activities.admin;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.e_station.R;
import com.example.e_station.activities.manager.CompleteOrderActivity;
import com.example.e_station.adapter.CustomHistoryAdapter;
import com.example.e_station.models.BookingModel;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;

public class TotalBookingActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    private ProgressDialog progressDialog;
    private ArrayList<BookingModel> bookingList = new ArrayList<>();
    RecyclerView recyclerView;
    TextView tvNodata;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_total_booking);

        initView();
    }
    private void initView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        mFireStore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        progressDialog = new ProgressDialog(this);
        recyclerView = findViewById(R.id.recyclerView);
        tvNodata = findViewById(R.id.tvNoData);

        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(TotalBookingActivity.this));

        getBookingDetails();
    }

    private void getBookingDetails() {
        mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL)
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException error) {

                        progressDialog.dismiss();
                        bookingList = new ArrayList<>();
                        for (QueryDocumentSnapshot document : queryDocumentSnapshots) {
                            if (document.getData().get("status").toString().matches("-1") || document.getData().get("status").toString().matches("2")) {
                                BookingModel bookingModel = document.toObject(BookingModel.class);
                                bookingModel.setDocId(document.getId());
                                bookingList.add(bookingModel);
                            }
                        }

                        if (bookingList.size() < 1) {
                            recyclerView.setVisibility(View.GONE);
                            tvNodata.setVisibility(View.VISIBLE);
                        }else{
                            recyclerView.setVisibility(View.VISIBLE);
                            tvNodata.setVisibility(View.GONE);
                        }

                        CustomHistoryAdapter customAdapter = new CustomHistoryAdapter(TotalBookingActivity.this, bookingList);
                        recyclerView.setAdapter(customAdapter);


                    }
                });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}