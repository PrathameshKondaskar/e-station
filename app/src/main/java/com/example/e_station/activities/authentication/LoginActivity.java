package com.example.e_station.activities.authentication;

import android.content.Context;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;


import com.bumptech.glide.Glide;
import com.example.e_station.R;
import com.example.e_station.fragments.LoginTabFragment;
import com.example.e_station.fragments.SignupTabFragment;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.FirebaseApp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;


public class LoginActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    FirebaseUser user;
    FirebaseFirestore mFirestore;
    ImageView coverpic;

    TabLayout tableLayout;
    ViewPager viewPager;
    FloatingActionButton  google;
    LoginAdapter loginAdapter ;
    float v =0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseApp.initializeApp(this);
        mAuth = FirebaseAuth.getInstance();
        user=mAuth.getCurrentUser();
        mFirestore = FirebaseFirestore.getInstance();

        tableLayout = findViewById(R.id.tab_layout);
        viewPager = findViewById(R.id.view_pager);
        google = findViewById(R.id.fabGoogle);
        coverpic = findViewById(R.id.coverpic);

        tableLayout.addTab(tableLayout.newTab().setText("Login"));
        tableLayout.addTab(tableLayout.newTab().setText("Sign up"));
        tableLayout.setTabGravity(TabLayout.GRAVITY_FILL);


        loginAdapter = new LoginAdapter(getSupportFragmentManager() ,this ,tableLayout.getTabCount());
        viewPager.setAdapter(loginAdapter);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tableLayout));


        google.setTranslationY(300);
        tableLayout.setTranslationY(300);

        google.setAlpha(v);
        tableLayout.setAlpha(v);


        google.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(400).start();
        tableLayout.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(400).start();

        tableLayout.setOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
//        fb.setBackgroundResource(R.drawable.facebook);

//

        Glide.with(LoginActivity.this).load(R.drawable.cargif3).into(coverpic);

    }


    private void updateUI(FirebaseUser user) {

        if(user!=null){
            Toast.makeText(LoginActivity.this, "Login Successfull.", Toast.LENGTH_SHORT).show();
//            startActivity(new Intent(LoginActivity.this, AdminMainActivity.class));
            finish();
        }

    }



    public class LoginAdapter extends FragmentPagerAdapter{
        private Context context;
        int totalTabs;

        public LoginAdapter(@NonNull FragmentManager fm,Context context ,int totalTabs) {
            super(fm);
            this.context = context;
            this.totalTabs = totalTabs;

        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            switch (position){
                case 0 :
                    LoginTabFragment loginTabFragment = new LoginTabFragment();
                    return loginTabFragment;

                case 1:
                    SignupTabFragment signupTabFragment = new SignupTabFragment();
                    return signupTabFragment;

                default:
                    return null;

            }

        }

        @Override
        public int getCount() {
            return totalTabs;
        }
    }
}