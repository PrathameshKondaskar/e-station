package com.example.e_station.activities.manager;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.activities.user.NewBookingActivity;
import com.example.e_station.models.BookingModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddVehicleActivity extends AppCompatActivity {

    TextInputEditText name,mobile,vehicleName,vehicleModel,rate;
    TextView date ,time;
    Button submit;
    AppCompatRadioButton radioButton;
    RadioGroup radioGroup;
    String paymentType ="",uTime ="",uDate="";
    // Initialize Firebase Auth
    private FirebaseAuth mAuth;
    private FirebaseFirestore mFireStore;
    UserSharedPreference userSharedPreference;
    ProgressDialog progressDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_vehicle);

        iniView();

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                int selectedType = radioGroup.getCheckedRadioButtonId();
                radioButton = findViewById(selectedType);
                paymentType = radioButton.getText().toString();
            }
        });

        uTime = getReminingTime();
        time.setText(uTime);
        uDate  = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault()).format(new Date());
        date.setText(uDate);
    }

    private void iniView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        name = findViewById(R.id.name);
        mobile = findViewById(R.id.mobile);
        vehicleModel = findViewById(R.id.vehicleModel);
        vehicleName = findViewById(R.id.vehicleName);
        rate = findViewById(R.id.rate);
        date = findViewById(R.id.date);
        time = findViewById(R.id.time);
        submit = findViewById(R.id.buttonSubmit);
        radioGroup = findViewById(R.id.radioGroup);
        userSharedPreference = new UserSharedPreference(this);
        progressDialog = new ProgressDialog(this);

        mAuth = FirebaseAuth.getInstance();
        mFireStore = FirebaseFirestore.getInstance();

        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime(time);
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addVehicle();
            }
        });


    }
    private String getReminingTime() {
        String delegate = "hh:mm aaa";
        return (String) DateFormat.format(delegate,Calendar.getInstance().getTime());
    }

    private void addVehicle() {
        String userName = name.getText().toString();
        String userMobile = mobile.getText().toString();
        String userVehicleName = vehicleName.getText().toString();
        String userVehicleModel = vehicleModel.getText().toString();
        String uRate = rate.getText().toString();
        uDate = date.getText().toString();
        uTime = time.getText().toString();

        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (userName.equals("")) {
            shouldCancelSignUp = true;
            focusView = name;
            name.setError("name is a required field");
        }

        if (userMobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a required field");
        }
        if (!userMobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. is a invalid");

        }
        if (mobile.length() != 10 || userMobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = mobile;
            mobile.setError("mobile no. must be of 10 digit");
        }
        if (userVehicleName.equals("")) {
            shouldCancelSignUp = true;
            focusView = vehicleName;
            vehicleName.setError("Vehicle Name is a required field");
        }
        if (userVehicleModel.equals("")) {
            shouldCancelSignUp = true;
            focusView = vehicleModel;
            vehicleModel.setError("Vehicle Model is a required field");
        }
        if (uRate.equals("")) {
            shouldCancelSignUp = true;
            focusView = name;
            name.setError("Rate is a required field");
        }
        if (paymentType.equals("")) {
            shouldCancelSignUp = true;
            focusView = radioGroup;
            Toast.makeText(AddVehicleActivity.this, "Please select the payment option", Toast.LENGTH_SHORT).show();
        }
        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else {
            progressDialog.show();
            progressDialog.setMessage("Booking");
            BookingModel bookingModel = new BookingModel("",mAuth.getUid(),userName,userMobile,randomNo(),userSharedPreference.getEstationId(),userSharedPreference.getEstationName(),userSharedPreference.getEstationAddress(),uDate,uTime,uRate,paymentType,"1",mAuth.getUid(),userVehicleName,userVehicleModel);
            saveBookingToFirebase(bookingModel);
        }

    }

    private void saveBookingToFirebase(BookingModel bookingModel) {
        mFireStore.collection(BookingModel.FIREBASE_COLLECTION_BOOKINGMODEL).add(bookingModel)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful())
                        {
                            if(progressDialog!=null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
//                            Toast.makeText(NewBookingActivity.this, "Power Station  added", Toast.LENGTH_SHORT).show();
//                            cleaAllValues();
                            showDialog("Booking Successful",2);
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(AddVehicleActivity.this, "Property not added", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("TAG", "onFailure: "+e.getMessage());
                    }
                });
    }

    public void showDialog (String msg , int type){
        SweetAlertDialog pDialog = new SweetAlertDialog(AddVehicleActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                finish();
                sDialog.cancel();
            }
        });
        pDialog.show();
    }

    public void getTime(TextView view){
        final String[] time = {""};
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal.set(Calendar.MINUTE, selectedMinute);
//                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                //checkCustomerPlanForThisTime(simpleDateFormat.format(cal.getTime()));
                view.setText(simpleDateFormat.format(cal.getTime()));

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }

    public String randomNo() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return "O-"+sb1.toString();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


}