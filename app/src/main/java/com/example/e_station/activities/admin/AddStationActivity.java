package com.example.e_station.activities.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.e_station.R;
import com.example.e_station.models.PowerStationModel;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

public class AddStationActivity extends AppCompatActivity {

    EditText stationName , stationAddress ;
    EditText latitude, longitude ;
    TextView from1,from2,from3,from4;
    TextView to1,to2,to3,to4;
    EditText rate1,rate2,rate3,rate4;
    Button add;
    ProgressDialog progressDialog;
    //Firebase
    FirebaseFirestore mFirestore;

    private ArrayList<PowerStationModel.Slots> slotsArrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_station);

        initViews();
    }

    private void initViews() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        mFirestore = FirebaseFirestore.getInstance();
        progressDialog = new ProgressDialog(this);
        stationName = findViewById(R.id.stationName);
        stationAddress = findViewById(R.id.stationAddress);
        latitude = findViewById(R.id.stationLat);
        longitude = findViewById(R.id.stationLong);
        from1 = findViewById(R.id.from1);
        from2 = findViewById(R.id.from2);
        from3 = findViewById(R.id.from3);
        from4 = findViewById(R.id.from4);
        to1 = findViewById(R.id.to1);
        to2 = findViewById(R.id.to2);
        to3 = findViewById(R.id.to3);
        to4 = findViewById(R.id.to4);
        rate1 = findViewById(R.id.rate1);
        rate2 = findViewById(R.id.rate2);
        rate3 = findViewById(R.id.rate3);
        rate4 = findViewById(R.id.rate4);
        add = findViewById(R.id.buttonSubmit);

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addStation();
            }
        });

        from1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        from2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        from3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        from4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        to1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        to2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        to3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
        to4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getTime((TextView) view);
            }
        });
    }


    public void addStation(){
       // setValues();
        final String name = stationName.getText().toString().trim();
        final String address = stationAddress.getText().toString().trim();
        final String lat = latitude.getText().toString().trim();
        final String lang = longitude.getText().toString();
        final String f1 = from1.getText().toString();
        final String f2 = from2.getText().toString();
        final String f3 = from3.getText().toString();
        final String f4 = from4.getText().toString();
        final String t1 = to1.getText().toString();
        final String t2 = to2.getText().toString();
        final String t3 = to3.getText().toString();
        final String t4 = to4.getText().toString();
        final String r1 = rate1.getText().toString();
        final String r2 = rate2.getText().toString();
        final String r3 = rate3.getText().toString();
        final String r4 = rate4.getText().toString();


        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (name.equals("")) {
            shouldCancelSignUp = true;
            focusView = stationName;
            stationName.setError("station name is a required field");
        }
        if (address.equals("")) {
            shouldCancelSignUp = true;
            focusView = stationAddress;
            stationAddress.setError("station address is a required field");
        }
        if (lat.equals("")) {
            shouldCancelSignUp = true;
            focusView = latitude;
            latitude.setError("latitude is a required field");
        }

        if (lang.equals("")) {
            shouldCancelSignUp = true;
            focusView = longitude;
            longitude.setError("longitude is a required field");
        }

        if (f1.equals("")) {
            shouldCancelSignUp = true;
            focusView = from1;
            from1.setError("its a required field");
        }
        if (f2.equals("")) {
            shouldCancelSignUp = true;
            focusView = from2;
            from2.setError("its a required field");
        }
        if (f2.equals("")) {
            shouldCancelSignUp = true;
            focusView = from3;
            from3.setError("its a required field");
        }
        if (f4.equals("")) {
            shouldCancelSignUp = true;
            focusView = from4;
            from4.setError("its a required field");
        }
        if (t1.equals("")) {
            shouldCancelSignUp = true;
            focusView = to1;
            to1.setError("its a required field");
        }
        if (t2.equals("")) {
            shouldCancelSignUp = true;
            focusView = to2;
            to2.setError("its a required field");
        }
        if (t3.equals("")) {
            shouldCancelSignUp = true;
            focusView = to3;
            to3.setError("its a required field");
        }
        if (t4.equals("")) {
            shouldCancelSignUp = true;
            focusView = to4;
            to4.setError("its a required field");
        }
        if (r1.equals("")) {
            shouldCancelSignUp = true;
            focusView = rate1;
            rate1.setError("its a required field");
        }
        if (r2.equals("")) {
            shouldCancelSignUp = true;
            focusView = rate2;
            rate2.setError("its a required field");
        }

        if (r3.equals("")) {
            shouldCancelSignUp = true;
            focusView = rate3;
            rate3.setError("its a required field");
        }
        if (r4.equals("")) {
            shouldCancelSignUp = true;
            focusView = rate4;
            rate4.setError("its a required field");
        }

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }
        else
        {
            slotsArrayList.add(new PowerStationModel.Slots(f1,t1,r1));
            slotsArrayList.add(new PowerStationModel.Slots(f2,t2,r2));
            slotsArrayList.add(new PowerStationModel.Slots(f3,t3,r3));
            slotsArrayList.add(new PowerStationModel.Slots(f4,t4,r4));

            PowerStationModel powerStationModel = new PowerStationModel(randomNo(),name,address,lat,lang,slotsArrayList);
            addStationToFirebase(powerStationModel);
        }

    }

    private void addStationToFirebase(PowerStationModel powerStationModel) {

        mFirestore.collection(PowerStationModel.FIREBASE_COLLECTION_POWERSTATION).add(powerStationModel)
                .addOnCompleteListener(new OnCompleteListener<DocumentReference>() {
                    @Override
                    public void onComplete(@NonNull Task<DocumentReference> task) {
                        if(task.isSuccessful())
                        {
                            progressDialog.dismiss();
                            Toast.makeText(AddStationActivity.this, "Power Station  added", Toast.LENGTH_SHORT).show();
//                            cleaAllValues();
                        }
                        else
                        {
                            progressDialog.dismiss();
                            Toast.makeText(AddStationActivity.this, "Property not added", Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.d("TAG", "onFailure: "+e.getMessage());
                    }
                });
    }

    public void getTime(TextView view){
        final String[] time = {""};
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                Calendar cal = Calendar.getInstance();
                cal.set(Calendar.HOUR_OF_DAY, selectedHour);
                cal.set(Calendar.MINUTE, selectedMinute);
//                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm a");
                //checkCustomerPlanForThisTime(simpleDateFormat.format(cal.getTime()));
                view.setText(simpleDateFormat.format(cal.getTime()));

            }
        }, hour, minute, false);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();

    }



    private void cleaAllValues() {
        stationName.setText("");
        latitude.setText("");
        longitude.setText("");
        from1.setText("");
        from2.setText("");
        from3.setText("");
        from4.setText("");
        to1.setText("");
        to2.setText("");
        to3.setText("");
        to4.setText("");
        rate1.setText("");
        rate2.setText("");
        rate3.setText("");
        rate4.setText("");
    }
    private void setValues() {
        stationName.setText("a");
        latitude.setText("1");
        longitude.setText("2");
        from1.setText("1");
        from2.setText("2");
        from3.setText("2");
        from4.setText("3");
        to1.setText("3");
        to2.setText("4");
        to3.setText("4");
        to4.setText("4");
        rate1.setText("6");
        rate2.setText("6");
        rate3.setText("43");
        rate4.setText("3");
    }

    public String randomNo() {
        char[] chars1 = "ABCDEF012GHIJKL345MNOPQR678STUVWXYZ9".toCharArray();
        StringBuilder sb1 = new StringBuilder();
        Random random1 = new Random();
        for (int i = 0; i < 6; i++) {
            char c1 = chars1[random1.nextInt(chars1.length)];
            sb1.append(c1);
        }
        return sb1.toString();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}