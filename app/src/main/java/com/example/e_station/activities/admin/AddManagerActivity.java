package com.example.e_station.activities.admin;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.e_station.R;
import com.example.e_station.UserSharedPreference;
import com.example.e_station.activities.MainActivity;
import com.example.e_station.activities.authentication.LoginActivity;
import com.example.e_station.activities.user.NewBookingActivity;
import com.example.e_station.adapter.CustomSpinnerAdapter;
import com.example.e_station.models.PowerStationModel;
import com.example.e_station.models.UserInfoModel;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.ArrayList;
import java.util.List;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class AddManagerActivity extends AppCompatActivity {

    private TextInputEditText mName,mMobile,mEmail,mPassword;
    private AutoCompleteTextView eStationSpinner;
    private Button add;
    private ProgressDialog progressDialog;
    private ImageView imageView;

    //Firebase
    FirebaseFirestore mFirestore;
    private FirebaseAuth mAuth;
    UserSharedPreference userSharedPreference;
    private StorageReference mStorageRef;

    int flag = 0;
    private static final int PICK_IMAGE_REQUEST = 1;
    private Uri mImageUri = null;
    public ArrayList<PowerStationModel> powerStationList= new ArrayList<>();
    public ArrayList<String> powerStationNameList = new ArrayList<>();
    String eStationame="" ,eStationId="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_manager);

        initView();

    }

    private void initView() {

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.back);

        mFirestore = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        userSharedPreference = new UserSharedPreference(this);
        progressDialog = new ProgressDialog(this);
        imageView = findViewById(R.id.imageProfile);
        mName = findViewById(R.id.name);
        mMobile = findViewById(R.id.mobile);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.password);
        add=findViewById(R.id.buttonSubmit);
        eStationSpinner = findViewById(R.id.eStationSpinner);
        mStorageRef = FirebaseStorage.getInstance().getReference();
        if(progressDialog!=null && !progressDialog.isShowing()) {
           // progressDialog.show();
        }

        getEstations();

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openFileChooser();
            }
        });

        eStationSpinner.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                eStationame = powerStationList.get(i).getName();
                eStationId = powerStationList.get(i).getId();
            }
        });

        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addManager();
            }
        });
    }
    private void openFileChooser() {

        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(intent, PICK_IMAGE_REQUEST);

    }

    public void addManager(){
        final String userEmail = mEmail.getText().toString().trim();
        final String userMobile = mMobile.getText().toString();
        final String userName = mName.getText().toString();
        final String userPassword = mPassword.getText().toString();
        Log.d("TAG", "addManager: "+eStationame);

        boolean shouldCancelSignUp = false;
        View focusView = null;

        if (userName.equals("")) {
            shouldCancelSignUp = true;
            focusView = mName;
            mName.setError("name is a required field");
        }
        if (userEmail.equals("")) {
            shouldCancelSignUp = true;
            focusView = mEmail;
            mEmail.setError("email is a required field");
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(userEmail).matches()) {
            shouldCancelSignUp = true;
            focusView = mEmail;
            mEmail.setError("email is not valid");
        }
        if (userPassword.equals("")) {
            shouldCancelSignUp = true;
            focusView = mPassword;
            mPassword.setError("Password is a required field");
        }

        if (userPassword.length() < 6) {
            shouldCancelSignUp = true;
            focusView = mPassword;
            mPassword.setError("Password must be greater than 6 letters");
        }
        if (userMobile.equals("")) {
            shouldCancelSignUp = true;
            focusView = mMobile;
            mMobile.setError("mobile no. is a required field");
        }
        if (!userMobile.matches("^(?:(?:\\+|0{0,2})91(\\s*[\\ -]\\s*)?|[0]?)?[789]\\d{9}|(\\d[ -]?){10}\\d$")) {
            shouldCancelSignUp = true;
            focusView = mMobile;
            mMobile.setError("mobile no. is a invalid");

        }
        if (userMobile.length() != 10 || userMobile.matches(".*[a-z].*")) {
            shouldCancelSignUp = true;
            focusView = mMobile;
            mMobile.setError("mobile no. must be of 10 digit");
        }
        if(eStationame.equalsIgnoreCase("")){
            shouldCancelSignUp = true;
            focusView = eStationSpinner;
            Toast.makeText(AddManagerActivity.this, "Please select the station", Toast.LENGTH_SHORT).show();
        }
        if (flag == 0) {
            shouldCancelSignUp = true;
            Toast.makeText(AddManagerActivity.this, "Image Not Selected", Toast.LENGTH_SHORT).show();
        }else

        if (shouldCancelSignUp) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        }else{


            progressDialog.setMessage("Adding Manger...");
            progressDialog.show();
            progressDialog.setCancelable(false);

            mAuth.createUserWithEmailAndPassword(userEmail,userPassword)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if(task.isSuccessful()){

                                String userId = mAuth.getUid();
                                final String path = System.currentTimeMillis()+"."+getExtenssion(mImageUri);
                                final StorageReference fileRef = mStorageRef.child("Manager Profile").child(path);


                                fileRef.putFile(mImageUri).continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
                                    @Override
                                    public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                                        if (!task.isSuccessful()){
                                            throw task.getException();
                                        }
                                        return fileRef.getDownloadUrl();
                                    }
                                }).addOnCompleteListener(new OnCompleteListener<Uri>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Uri> task) {
                                        if (task.isSuccessful()){
                                            Uri downUri = task.getResult();
                                            Log.d("PATH", "onComplete: Url: "+ downUri.toString());


                                            UserInfoModel userInfoModel = new UserInfoModel(userName,userEmail,userPassword,userMobile,"","2",downUri.toString(),"",userId,eStationId,eStationame,"","");

                                            mFirestore.collection(UserInfoModel.FIREBASE_COLLECTION_USERINFO).document(userId)
                                                    .set(userInfoModel)
                                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                                        @Override
                                                        public void onComplete(@NonNull Task<Void> task) {
                                                            progressDialog.dismiss();
                                                            FirebaseAuth.getInstance().signOut();
                                                            userSharedPreference.clearSharedPreferences();
                                                            showDialog("Manager Added");
                                                            //Toast.makeText(AddManagerActivity.this, "Manager added", Toast.LENGTH_SHORT).show();
                                                        }
                                                    })
                                                    .addOnFailureListener(new OnFailureListener() {
                                                        @Override
                                                        public void onFailure(@NonNull Exception e) {
                                                            progressDialog.dismiss();
                                                            Toast.makeText(AddManagerActivity.this, "Manager not added", Toast.LENGTH_SHORT).show();
                                                        }
                                                    });

                                        }
                                    }
                                });



                            }else {
                                progressDialog.dismiss();
                                if (task.getException() instanceof FirebaseAuthUserCollisionException)
                                    Toast.makeText(AddManagerActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });





        }
    }

    public void getEstations(){

        mFirestore.collection(PowerStationModel.FIREBASE_COLLECTION_POWERSTATION)
                .get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {

                        if(!queryDocumentSnapshots.isEmpty()){


                            List<PowerStationModel> types = queryDocumentSnapshots.toObjects(PowerStationModel.class);
                            powerStationList.addAll(types);

                            for (PowerStationModel powerStationModel:powerStationList) {

                                powerStationNameList.add(powerStationModel.getName());
                            }

                            if(progressDialog!=null && progressDialog.isShowing()) {
                                progressDialog.dismiss();
                            }
                            CustomSpinnerAdapter customAdapter = new CustomSpinnerAdapter(AddManagerActivity.this, android.R.layout.simple_list_item_1, powerStationNameList);
                            eStationSpinner.setAdapter(customAdapter);

                        }

                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(AddManagerActivity.this, "Please check your internet connection", Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null) {
            mImageUri = data.getData();
            Log.d("path", mImageUri.toString());
            imageView.setImageURI(mImageUri);
            //Picasso.with(this).load(mImageUri).into(mImageView);
            //Bitmap bitmap = (Bitmap)data.getExtras().get(data.getData().toString());
            //mImageView.setImageBitmap(bitmap);
            imageView.buildDrawingCache();
            flag = 1;

        }
    }
    private String getExtenssion(Uri uri) {
        ContentResolver cr = getContentResolver();
        MimeTypeMap mime = MimeTypeMap.getSingleton();
        return mime.getExtensionFromMimeType(cr.getType(uri));

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void showDialog (String msg ){
        SweetAlertDialog pDialog = new SweetAlertDialog(AddManagerActivity.this, SweetAlertDialog.SUCCESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#A5DC86"));
        pDialog.setTitleText(msg);
        pDialog.setCancelable(false);
        pDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
            @Override
            public void onClick(SweetAlertDialog sDialog) {
                finish();
                sDialog.cancel();
            }
        });
        pDialog.show();
    }


}